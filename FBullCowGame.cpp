#pragma once
#include "FBullCowGame.h"
#include <map>
// Make syntax Unreal friendly
#define TMap std::map

using int32 = int;
using FString = std::string;

void FBullCowGame::Reset()
{
	MyCurrentTry = 1;
	bIsGuesscorrect = false;
	return;
}

FBullCowGame::FBullCowGame() //default constructor
{
	Reset();
}

FBullCowGame::FBullCowGame(int32 Length)  //constructor with word length passed in
{
	Reset();
	SetHiddenWordByLength(Length);
}

int32 FBullCowGame::GetMaxTries() const 

{ 
	TMap<int32, int32> WordLengthToMaxTries { {3, 6}, {4, 7}, {5,9}, {6, 12} };
	return WordLengthToMaxTries[MyHiddenWord.length()]; 
}

int32 FBullCowGame::GetCurrentTry() const { return MyCurrentTry; }

int32 FBullCowGame::GetHiddenWordLength() const
{
	return MyHiddenWord.length();
}

void FBullCowGame::SetHiddenWordByLength(int32 Length)
{
	TMap<int32, FString> HiddenWordsMappedByLength{ { 4, "word" },{ 5, "strap" },{ 6, "planet" } };
	MyHiddenWord = HiddenWordsMappedByLength[Length];
}

bool FBullCowGame::IsGameWon() const
{
	return bIsGuesscorrect;
}


EGuessStatus FBullCowGame::CheckguessValidity(FString Guess) const
{
	if (!FBullCowGame::IsIsogram(Guess))//if the guess isn't an isogram, error
	{
		return EGuessStatus::Not_Isogram;
	}
	else if (!IsLowerCase(Guess))//if the guess isn't all lowercase
	{
		return EGuessStatus::Not_Lowercase;
	}
	else if (Guess.length() != GetHiddenWordLength())//if the length is wrong
	{
		return EGuessStatus::Wrong_Length;
	}
	else
	{
		return EGuessStatus::OK;
	}
}

//receives valid guess, increments turn and returns bull and cow count
FBullCowCount FBullCowGame::SubmitValidGuess(FString Guess)
{
	
	MyCurrentTry++;

	//setup return struct
	FBullCowCount BullCowCount;

	int32 HiddenWordLength = MyHiddenWord.length();
	int32 GuessWordLength = Guess.length();
	//loop through all letter in guess
	for (int32 i = 0; i < GuessWordLength; i++)
	{
		//compare letters against hidden word 
		for (int32 j = 0; j < HiddenWordLength; j++)
		{
			//if they match then
			if (Guess[i] == MyHiddenWord[j])
			{	
				if (i == j)
				{
					//increment bulls if in same palce
					BullCowCount.Bulls++;
				}
				else
				{
					// increment cows if not
					BullCowCount.Cows++;
				}
			}
		}
	}
	if (BullCowCount.Bulls == HiddenWordLength)
	{
		bIsGuesscorrect = true;
	}
	return BullCowCount;
}

bool FBullCowGame::IsIsogram(FString Guess) const
{
	//treat 0 and 1 letter words as isograms
	if (Guess.length() <= 1) { return true; }

	TMap<char, bool> LetterSeen;
	for (auto Letter : Guess)	//for all letters of the word
	{
		Letter = tolower(Letter); //handle mixed case Guesses
		if (LetterSeen[Letter])  // if seen this letter before, used LetterSeen[Letter] == true
		{
			return false; //not an isogram
		}
		else
		{
			LetterSeen[Letter] = true;
		}
	}

	return true; // for example if /0 is the guess
}

bool FBullCowGame::IsLowerCase(FString Guess) const
{
	//loop through the word, checking if any letter is not lowercase
	for (auto Letter : Guess)
	{
		if (!islower(Letter))
		{
			return false;
		}
	}
	return true;
}

/* The game logic (no view code or direct user interaction)
	The game is a word guessing game based on Mastmind
*/
#pragma once
#include <string>

//Make the syntax Unreal friendly
using FString = std::string;
using int32 = int;


struct FBullCowCount
{
	int32 Bulls = 0;
	int32 Cows = 0;
};

enum class EGuessStatus
{
	Invalid_Status,
	OK,
	Not_Isogram,
	Wrong_Length,
	Not_Lowercase,
};

class FBullCowGame 
{
public:
	FBullCowGame();
	FBullCowGame(int32);
	int32 GetMaxTries() const;
	int32 GetCurrentTry() const;
	int32 GetHiddenWordLength() const;

	void SetHiddenWordByLength(int32);

	bool IsGameWon() const;
	EGuessStatus CheckguessValidity(FString) const; 

	void Reset();  
	FBullCowCount SubmitValidGuess(FString);

private:
	int32 MyCurrentTry;
	FString MyHiddenWord;
	bool bIsGuesscorrect;
	

	bool IsIsogram(FString Guess) const;
	bool IsLowerCase(FString Guess) const;
};
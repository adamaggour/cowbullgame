/* This is the console executable that makes use of FBullCowGame class. 
It's the view in the MVC pattern and is responsible for user interaction.
for game logic see the FBullCowGame class
*/
#pragma once

#include <iostream>
#include <string>
#include "FBullCowGame.h"

// make syntax Unreal friendly
using FText = std::string;
using FString = std::string;
using int32 = int;

//function prototypes
void PrintIntro();
FText GetValidGuess();
int32 GetValidWordLength();
void RepeatGuess(FText Guess);
void PlayGame();
bool AskToPlayAgain();
void PrintGameSummary(bool GameWon);

FBullCowGame BCGame;  // instantiate a new game for use and re-use across games
FString RequestedWordLength = "";

int main() {
	
	do
	{
		PlayGame();
	} while (AskToPlayAgain());
	return 0; //exit app
}

//introduce the game
void PrintIntro()
{
	std::cout << "\n\nWelcome to Bulls and Cows, a fun word game.\n";
	std::cout << "Can you guess the " << BCGame.GetHiddenWordLength();
	std::cout << " letter isogram I'm thinking of?\n";
	std::cout << std::endl;
	return;
}

int32 GetValidWordLength()
{
	FString WordLengthString = "";
	std::cout << "\nWhat word length would you like to use?";
	do 
	{
		std::cout << "\nEnter 4, 5 or 6:";
		std::getline(std::cin, WordLengthString);
	} while (!((WordLengthString == "4") || (WordLengthString == "5") || (WordLengthString == "6")));
	return std::stoi(WordLengthString);  //return the selected word length as an integer
	
}

FText GetValidGuess()
{
	int32 CurrentTry = BCGame.GetCurrentTry();
	FText Guess = "";
	EGuessStatus Status = EGuessStatus::Invalid_Status;
	
	do
	{
		std::cout << "Try #" << CurrentTry << " of " << BCGame.GetMaxTries() << ". Enter your guess:";
		std::getline(std::cin, Guess);
		Status = BCGame.CheckguessValidity(Guess);

		switch (Status)
		{
		case EGuessStatus::Wrong_Length:
			std::cout << "Please enter a " << BCGame.GetHiddenWordLength() << " letter word.\n\n";
			break;
		case EGuessStatus::Not_Isogram:
			std::cout << "That's not an Isogram. Please don't repeat letters.\n\n";
			break;
		case EGuessStatus::Not_Lowercase:
			std::cout << "Please enter all lowercase letters.\n\n";
			break;
		default:
			break;
		}
	} while (Status != EGuessStatus::OK); //keep looping till no errors
	return Guess;
}

//play a single game to completion
void PlayGame()
{
	BCGame = FBullCowGame(GetValidWordLength());

	PrintIntro();

	int32 MaxTries = BCGame.GetMaxTries();
	FText Guess = "";
	

	//loop asking for guesses while game not won and tries remain
	while(!BCGame.IsGameWon() && BCGame.GetCurrentTry() <= MaxTries)
	{
		Guess = GetValidGuess(); 

		
		//submit valid guess to game and receive + display counts
		FBullCowCount BullCowCount = BCGame.SubmitValidGuess(Guess);
		
		RepeatGuess(Guess);
		std::cout << "Bulls = " << BullCowCount.Bulls;
		std::cout << ". Cows = " << BullCowCount.Cows << std::endl << std::endl;
		
	}
	//Game is won or max tries reached, sumarize appropriately
	PrintGameSummary(BCGame.IsGameWon());

	return;
}

//repeat the guess back to the player
void RepeatGuess(FText Guess)
{
	std::cout << "Your guess was: " << Guess << std::endl;
}

void PrintGameSummary(bool GameWon)
{
	if (GameWon)
	{
		std::cout << "\nCongratulations! You guessed the word correctly!\n";
	}
	else
	{
		std::cout << "\nI'm sorry, you failed to guess the hidden word within the given number of tries :(\n";
	}
		
	return;
}

bool AskToPlayAgain()
{
	FText Response = "";
	std::cout << "Do you want to play again? (y/n):";
	std::getline(std::cin, Response);

	return ((Response[0] == 'y') || (Response[0] == 'Y'));
	
}
